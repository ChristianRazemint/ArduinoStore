<?php session_start();
error_reporting(E_ALL);
ini_set('display_errors', 'on');

  require 'admin/config.php';
  require 'functions.php';

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      $correo = $_POST['correo'];
      $password = $_POST['password'];

      $errores = '';

      //validar campos de texto
      if (empty($correo) || empty($password)) {
        $errores .= '<li class="error">Por favor rellene todos los campos</li>';
      } else {
        //si el usuario vale verga
        $conexion = conexion($bd_config);
        $statement = $conexion->prepare('SELECT * FROM usuarios WHERE correo = :correo');
        $statement->execute([
          ':correo' => $correo
        ]);
        $resultado = $statement->fetchAll();
        if ($resultado != false) {
          $errores .= '<li class="error">Este usuario ya existe</li>';
        }

      }

      if ($errores == '') {
        $conexion = conexion($bd_config);
        $statement = $conexion->prepare('INSERT INTO usuarios id, correo, password
        VALUES(null, :correo, :password)');
        $statement->execute([
          ':correo' => $correo,
          ':password' => $password
        ]);
        header('location '.RUTA.' login.php');
      }

  }


  require 'views/registro.view.php';

 ?>
